---
title: Vengeance
tags:
 - sketch
hidden: true
---
"No! Shut Up" I sat here all day thinking of you and wishing you well. I could have done anything else, but no just no. That is not okay, what you did is not fair. But who am I kidding, none of this can ever be said. It's all circumstantial. Even if I ended up in the exact situation I want and got to say what I want to say. Would I really be able to do it? Am I even right? When everyone insists I am, that must mean something. But not something enough for me to believe in that dream. That dream of expressing everything I ever wanted you to know in the perfect setting. 

