---
title: conversations
tags:
 - sketch
hidden: true
---
"No I can't say that I do".
"I just told you everything you need to know!"
"That doesn't change the fact that you are who you are."
"How does that help me?"
"It was never supposed to, you're help isn't with me."
"Can't you ever just think about me?"
"Yeah all the time, when it helps me."
"The world doesn't just revolve around you."
"To me it does, who else could it revolve around? I am right here."
