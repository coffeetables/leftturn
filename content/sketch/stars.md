---
title: Stars Below You
tags:
 - sketch
hidden: true
---
 You know those bright flashes of light in the sky. The ones that twinkle as if no one is watching. Imagine if they could continue faster then you would have ever expected. When you finally get around to looking up at night, it's no longer the night sky you see. No not that at all. 
 The fastest twinkle is glaring at you from one direction. Somewhere to your right or our left or maybe behind you. The funny thing is that you know exactly why it's so bright and fast, one could be the effect of the other. It is just going on and on. It stands out like an obnoxious headlight in a car's mirror. There is only one flash, one twinkle, one beam that could be so glaring. The heavens just planted your internal	compass. If someone were to ask you, the answer would be so simple, "It's that way", you would say, with a finger pointing in the right direction.
 If you started to look around more, there would of course be that one obnoxious glare from one point in the sky. But you would be able to definitively make out the other points and dots - glaring, twinkling points and dots. 
