---
title: Over and Over and Over Again
tags:
 - story
 - romance
 - addiction
---
I don't know what it is about you that makes me feel this way. There are the little things that make me smile and the big things that keep me smiling. I think theres something important that I'm missing, but I'm not really sure what it is.

You're the first person I see in every room I enter and the last person I want to see whenever I'm leaving. They keep telling me that we can do it - that we're going to win, face the world. I'm almost starting to believe them. Its so easy to keep that belief to insist that the world works this way - the same world thats stopping us.


"Come on dude drink up quickly, we can't stay here forever"

What can I say, its not all that bad. The fifth drink today is a milestone, this is the first time I've had more than four in the last ... two days - Its usually more. But I have to give myself some credit, I've just gone through the most horrific day. Although I guess it can't be as bad as the days before today, judging by my numb feeling on each day. In fact, I'm doing quite well today, the feeling is much less today. But it's only around 7.

I respond, "I think I'll get another, what are we even leaving for?". Well it's been good so far, I never promised about the future.

Claire, frustrated, says, "Isn't this you're third today? Lets just go, we talked about this. I got to go meet my mom and I thought you had to get home to study."

I might stretch the truth a bit to my friends, I can't let them get too close. Two makes a big difference.

"No that was a lie, just trying to look busy. You go on, I'll probably stay for a bit", I tell my friends.

With a couple grunts, I watch them walk towards the door. I think about the test I'm about to fail, let out a satisfying sigh, and take a sip.

At this point I think its been a while, and its so great to not have to deal with my life anymore. I feel like I could do anything. Maybe today is the day I try to fly. I no longer feel bad that the seats around me are empty. And you know what, the bar is still full, so I don't see the problem. There is still conversation in the air and I'm happy to not contribute.

The interesting thing about people is that they never know when others are tired. You would think the tired eyes, forced smile, and monotonous tone could give a hint. It's just so easy to leave, I'm  not even expecting a goodbye. Sometimes it feels good when they stay, I wish I could let them know that. Either their just sitting there talking or even worse, their standing there silent. I can deal with the stories and the information, just not the silence. My thoughts are fine to listen to, thats never been the issue; No I think its the consistent feeling that they have something better to do, someone more important to be.

It happened like a scene in one of those movies, where the heros walk into a room and immediately find the person to talk to and somehow that person has a problem that the heros are in the perfect position to solve. Since I was sitting down at the time and I wasn't even paying attention to the door, you can probably guess who I am in this analogy. I couldn't tell you if she was pretty or if they glowed in the light. I'm not sure why, but she is the type of person to catch your attention. I'm almost certain everyone in the room probably noticed her walk into the room, everyone except for me.

As I was sipping my latest drink, I noticed someone slip into my view. I felt a tap on my shoulder and looked up into a girl's eyes. She had this child-like twinkle in her eye and looked at me with a smile that I wish could be on my face.

"Hey, your ring", she says while pointing at my pointer finger.

"What about it?", I reply.

"Well I have the same one, look", she holds out her hand and points to her ring finger as if she just got married.

"Your partner and I must have the same taste", I say with an attempt at putting a small smile on my face.

She starts laughing and shakes her head down, "Ohh no no I just bought the ring for myself. Where did you get yours?".

"I got mine while on a trip with a couple friends, its cool that we picked the same design. I wonder if its a coincidence", I explain.

She then sits down next to me making the booth feel less empty, not that I wasn't doing good on my own. I was thinking of things to say and ways to make the conversation flow. A weird experience for me to even care that a dialogue should exist.

"So why are you sitting here?", she asks. I thank her in my head for doing exactly what I was planning to do.

"I came here with some friends and they all ended up leaving."

"That sounds so sad."

"Well I chose to stay here I could have gone too."

"So you chose this misery for yourself?"

"No I wanted this ... so yeah this was my choice"

"I've been thinking about how some groups work. I go out with my friend group, because its comfortable. Their right there in my head when we make some sort of plan. I could go with anyone else, but its just easier to go with them. "
