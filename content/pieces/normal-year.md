---
title: The Normal Year
tags:
 - speech
 - covid
---
How many times have you heard, "this year wasn't normal"?
If this year wasn't normal then how did I go to school like every other year?
This year was so normal that I got to meet some phenomenal new people.
There was nothing wierd about getting homework every night and taking tests every week.
Working with my friends on cool projects was the most normal thing we could do.

