---
title: Magic isn't magical
tags:
 - story
 - magic
 - fantasy
---
Listening to a professor attempt to shock their students through a lecture is always the highlight of my week.

"While learning magic throughought your life you may have come to the incorrect conclusion that magic is mysterious, mystical, or maybe even magical."
"But it's really quite simple. Its a combination of science and technology." 
"How do we know this? How can we possibly understand magic?"
"Because we built it, we built the first version of it. The only problem we have now ... whats changed?"
"We know how it started, there is extensive research on how could go grow, but what about what its thinking?"

Shawn looked around at everyone else in the lecture hall. One classmate caught his mind, she was effortlessly twirling a floating pen while taking notes. Telekinesis is not really special by itself, he wouldn't have been surprised if every person in the class was capable of moving a pen. But people are rarely able to do it well enough that its more efficient than just moving the item manually. It usually requires a lot of concentration, and even then magic has very little direct force on an object. He remembered running through the numbers the other day in physics. The girl was currently maintaining a constant force against gravity through sheer will and concentration and then visualizing the movements she wanted of the pen. Shawn couldn't see any drawing on her desk that was helping her, so it was impressive that she was doing all this in her head. 

"We know very little about the history of magic and what its thinking. As every one of you knows the most we know about it is how to use it. You have learned since you were young, that magic is just instruction. Leaving the details of a spell or instruction open to interpretation can result in surprises. If you have done any experimentation, you may have learned that those surprises aren't always bad and many times are better than the result you would have wanted."

*"Shawn!! How could you hurt your cousin like that?!", his mom yelled at him. "I didn't mean to, the fire just started", he replied. "What how thats... thats not possible", his mom says while looking at him with a confused look on her face.* 

"Shawn, can you please share a magical experiment you have conducted", Dr. Brahn asks.

Shawn jolts up from his memory upon hearing his name. The professor looks confused at the this reaction, but doesn't seem offended. Shawn comes up with a good example and answers the question, "
