---
title: Family in friends
tags:
 - story
 - friendship
 - family
---
  I used to tell people that the only way to my heart was food. Whenever my brother came home, he would always have in his hand a plump donut. A generous action from the bottom of his heart. He would then continue to request some help with his homework or that I take out the trash. How could I say no? This gracious human being just brought me a fat slice of the greatest desert ever invented.
  Now some people would have you believe the relationship's are the most important part of your life. Something something humans are social creatures and whatever. I wish I could just stand at the top of a giant building with a microphone in my hand and yell at the entire world. Something very simple really, "relationships are a choice." Some of us choose connection, others choose against it, and a few have no choice. I would like to think I am in the middle category. I have a friend who believes she has no choice, but only in between the different friends that come and go. The other times she tries very hard to impress. In the end, I guess she really does not have a choice.    
  "Whatcha doin?", I hear from the corner of my room. The only person who ever wants to talk to me walks in.
  "I don't know akka, well I guess I'm hanging out with you now," I reply to my older sister, May, who is probably the most exciting person in my life at the moment. 
  "Ugh Mathew, I love how you hate tradition and culture and you still continue call me that."
  "Well you're supposed to be special, I only have one annoying brat in my life."
  "Very creative, that is the second time you've used that this week. You need to up your game dude."
  "Ooh she's counting, I guess that one is working then. So what's up?," I say as my phone rings notifying me of a message.
  "Oh wow you're getting messages now? I'm impressed."
  "Ha you wish, no it's just an email from my counselor."
  "Oh whatever, we'll work on you later. So I just finished my homework, are you down to go outside and play with Riku?" That would be my obnoxious family dog.
  "mmm I'm not sure."
  "Why not? You've been holed up in here all day."
  "Yeah as usual, why should today be any different? Consistency is key."
  "Just come on, you need some social activity even if it's just me."
  "Okay okay I'll be down give me a couple minutes."



